module.exports = {
	extends   : [ 'airbnb' ],
	root      : true,
	rules     : {
		'react/jsx-filename-extension' : 'off',
		'react/prop-types'             : 'off',
		'react/jsx-props-no-spreading' : 'off'
	},
	globals : {
		fetch : false
	}
	// extends: '@react-native-community',
};

import React from 'react';
import * as eva from '@eva-design/eva';
import {
  ApplicationProvider,
  Layout,
  Button,
  Divider,
  List,
  ListItem,
} from '@ui-kitten/components';
import theme from './styles/theme.json'; // <-- Import app theme
import mapping from './styles/mapping.json';
import * as rssParser from 'react-native-rss-parser';
import TrackPlayer from 'react-native-track-player';

export default () => {
  const [podcast, setData] = React.useState({
    title: '',
    episodes: [],
  });

  const start = async (episode) => {
    await TrackPlayer.setupPlayer();

    await TrackPlayer.add(episode);

    await TrackPlayer.play();

    setTimeout(async () => {
      await TrackPlayer.stop();
    }, 15000);
  };

  React.useEffect(() => {
    fetch('https://anchor.fm/s/1eb8a40c/podcast/rss')
      .then((response) => response.text())
      .then((responseData) => rssParser.parse(responseData))
      .then((rss) => {
        const body = {
          title: rss.title,
          episodes: rss.items
            .map((item) => ({
              id: item.id,
              title: item.title,
              artwork: item.itunes.image,
              description: item.description
                .replace('<p>', '')
                .replace('</p>', ''),
              url: item.enclosures[0].url,
              artist: rss.title,
            }))
            .reverse(),
        };

        setData(body);
      });
  }, []);

  const playEpisode = (audio) => {
    start(audio);
  };

  const renderItemAccessory = ({ episode }) => (
    <Button onPress={() => playEpisode(episode)} size="tiny">
      Play
    </Button>
  );

  const renderItem = ({ item }) => (
    <ListItem
      title={item.title}
      description={item.description}
      ItemSeparatorComponent={Divider}
      accessoryRight={() => renderItemAccessory({ episode: item })}
    />
  );

  return (
    <ApplicationProvider
      {...eva}
      theme={{ ...eva.dark, ...theme }}
      customMapping={mapping}>
      <Layout
        style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <List
          style={{ width: '100%' }}
          data={podcast.episodes}
          renderItem={renderItem}
        />
      </Layout>
    </ApplicationProvider>
  );
};
